package com.DenCompany.forkedrepository.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"pageNo", "lines"})
public class Page {
    int pageNumber;
    String[] lines;

    public Page(String[] lines, int number) {
        this.lines = lines;
        this.pageNumber = number;
    }

    public String getLine(int numLine) {
        return lines[numLine];
    }

    @JsonProperty("pageNo")
    public int getNumber() {
        return pageNumber;
    }

    public String[] getLines() { return lines; }
}
