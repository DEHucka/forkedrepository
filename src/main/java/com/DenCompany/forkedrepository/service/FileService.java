package com.DenCompany.forkedrepository.service;

public interface FileService {
    String read(String path);
}
