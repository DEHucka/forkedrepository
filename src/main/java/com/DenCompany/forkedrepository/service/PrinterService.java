package com.DenCompany.forkedrepository.service;

import com.DenCompany.forkedrepository.model.Book;

public interface PrinterService {
    void printBook(Book book, int numPage, int countOfPages);
}
