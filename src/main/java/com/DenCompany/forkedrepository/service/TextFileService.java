package com.DenCompany.forkedrepository.service;

import java.io.FileReader;
import java.io.IOException;

public class TextFileService implements FileService{

    @Override
    public String read(String path) {
        String text = "";
        try (FileReader reader = new FileReader(path)) {
            int c;
            while ((c = reader.read()) != -1) {
                text += (char) c;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return text;
    }
}
