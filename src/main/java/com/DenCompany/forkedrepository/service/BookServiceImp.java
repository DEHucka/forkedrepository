package com.DenCompany.forkedrepository.service;

import com.DenCompany.forkedrepository.model.Book;

public class BookServiceImp implements BookService {
    BookPagesService bookPagesService;

    public BookServiceImp(BookPagesService bookPagesService) {
        this.bookPagesService = bookPagesService;
    }

    @Override
    public Book createBook(String path, int countOfLines, int countOfSymbols) {
        return new Book(countOfLines, countOfSymbols, bookPagesService.create(path, countOfLines, countOfSymbols));
    }
}
