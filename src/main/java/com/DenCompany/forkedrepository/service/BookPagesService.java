package com.DenCompany.forkedrepository.service;

import com.DenCompany.forkedrepository.model.Page;

import java.util.List;

public interface BookPagesService {
    List<Page> create(String path, int countOfLines, int countOfSymbols);
}
