package com.DenCompany.forkedrepository.service;

import com.DenCompany.forkedrepository.model.Book;

public interface BookService {
    Book createBook (String path, int countOfLines, int countOfSymbols);
}
