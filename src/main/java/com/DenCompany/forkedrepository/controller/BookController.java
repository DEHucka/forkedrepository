package com.DenCompany.forkedrepository.controller;


import com.DenCompany.forkedrepository.model.Page;
import com.DenCompany.forkedrepository.model.Book;
import com.DenCompany.forkedrepository.service.BookService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.*;

public class BookController {
    BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    public Page getPage(String query) {
        Map<String, String> params = splitQuery(query);
        String pageNumber = params.getOrDefault("pageNo","null");
        String filePath = params.getOrDefault("path","null");

        Book book = bookService.createBook(filePath, 3, 10);
        return book.getPages().get(Integer.parseInt(pageNumber) - 1);
    }

    Map<String, String> splitQuery(String query) {
        Map<String, String> map = new HashMap<>();
        for (String element : query.split("&")
        ) {
            String[] keyValue = element.split("=");
            map.put(keyValue[0], decode(keyValue[1]));
        }
        return map;
    }

    private static String decode(final String encoded) {
        try {
            return encoded == null ? null : URLDecoder.decode(encoded, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 is a required encoding", e);
        }
    }
}
