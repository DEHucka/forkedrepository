package com.DenCompany.forkedrepository;

import com.DenCompany.forkedrepository.controller.BookController;
import com.DenCompany.forkedrepository.service.BookPagesServiceImp;
import com.DenCompany.forkedrepository.service.TextFileService;
import com.DenCompany.forkedrepository.service.BookServiceImp;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

class Application {

    public static void main(String[] args) throws IOException {
        System.out.println("Запуск сервера");
        int serverPort = 8000;
        HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);
        BookController bookController = new BookController(new BookServiceImp(new BookPagesServiceImp(new TextFileService())));
        ObjectMapper objectMapper = new ObjectMapper();

        server.createContext("/api/books/id", (exchange -> {
            if ("GET".equals(exchange.getRequestMethod())) {
                String respText = objectMapper.writeValueAsString(bookController.getPage(exchange.getRequestURI().getRawQuery()));
                exchange.sendResponseHeaders(200, respText.getBytes().length);
                OutputStream output = exchange.getResponseBody();
                output.write(respText.getBytes());
                output.flush();
            } else {
                exchange.sendResponseHeaders(405, -1);// 405 Method Not Allowed
            }
            exchange.close();
        }));

        server.setExecutor(null); // creates a default executor
        server.start();
    }
}